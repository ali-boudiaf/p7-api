package fr.insee.p7springboot.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import fr.insee.p7springboot.service.KeycloakService;

@Component
public class KeycloakInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(KeycloakInterceptor.class);
	@Autowired
	private KeycloakService keycloakService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
//		String token = request.getHeader("authorization");
//		HttpSession session = request.getSession(true);
//		try {
//			keycloakService.verifierToken(token, session);
//		}
//		catch (IllegalArgumentException e) {
//			logger.error("Erreur à la verification du token ", e);
//		}
		return true;

	}
}
