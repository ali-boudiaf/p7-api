package fr.insee.p7springboot.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private KeycloakInterceptor keycloakInterceptor;

	// Ajoute l'intercepor à la config
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(keycloakInterceptor);
	}
}
