package fr.insee.p7springboot.service;

import javax.servlet.http.HttpSession;

public interface KeycloakService {

	boolean verifierToken(String token, HttpSession session);
}
