package fr.insee.p7springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.insee.p7springboot.dao.DrInseeDao;
import fr.insee.p7springboot.service.DrInseeService;

@Service
public class DrInseeServiceImpl implements DrInseeService {

	@Autowired
	private DrInseeDao drInseeDao;

	@Override
	public DrInseeDao getDao() {
		return drInseeDao;
	}

}
