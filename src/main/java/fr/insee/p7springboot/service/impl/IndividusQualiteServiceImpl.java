package fr.insee.p7springboot.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.insee.p7springboot.dao.IndividusQualiteDao;
import fr.insee.p7springboot.service.IndividusQualiteService;

@Service
public class IndividusQualiteServiceImpl implements IndividusQualiteService {
	
	@Autowired
	private IndividusQualiteDao individusQualiteDao;

	@Override
	public IndividusQualiteDao getDao() {
		return individusQualiteDao;
	}

}
