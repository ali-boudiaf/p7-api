package fr.insee.p7springboot.service.impl;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.insee.p7springboot.model.Utilisateur;
import fr.insee.p7springboot.service.KeycloakService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

@Service
public class KeycloakServiceImpl implements KeycloakService {

	private static final Logger logger = LoggerFactory.getLogger(KeycloakServiceImpl.class);

	public static final String KEYCLOAK_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtYyTD5Q5iBjWhHVvubSwER3x+GAlZPmE1dqGqPz+FmStf5Kite3Qi5GlqYxCCddQG9J5oCEGP5ym+3NOHVZdhWXX+n9e2TqyjKXX8FnkKouMr3cSoXKA4Q8KAlbvRZoMMesyMELWI34+X+4WUGuAfkr7ZpMml9eAiWCTN6t4MjAgby3Xt6uy/UWoaOIBa3uIlS7uALKJ/VrjYtfoBz7SZzPPO9JAIG9YCPLONHiA02/3gKo2tNzPr0mnHQSn72/sEl0T4ESHdV6qxhrOEOvZ3J1ugOzyF6s4VpTM05YWH999RlcyPuVUU1brkQpB+sGMb+rnXl9VCDqz/AJ7EME2NQIDAQAB";

	@Override
	public boolean verifierToken(String token, HttpSession session) {
		if (StringUtils.isEmpty(token)) {
			throw new IllegalArgumentException("Le token doit être renseigné");
		}
		try {
			logger.info("Verification du token " + token);
			PublicKey publicKey = stringKeyToPublicKey();
			String jwt = token.substring(7);
			Jws<Claims> parseClaimsJws = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(jwt);
			session.setAttribute("utilisateur", jwtToUtilisateur(parseClaimsJws));
		}
		catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.error("Pb signature ", e);
			return false;
		}
		return true;
	}

	private PublicKey stringKeyToPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		byte[] publicBytes = Base64.decodeBase64(KEYCLOAK_PUBLIC_KEY);
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicBytes);
		return keyFactory.generatePublic(publicKeySpec);
	}

	private Utilisateur jwtToUtilisateur(Jws<Claims> parseClaimsJws) {
		Utilisateur utilisateur = new Utilisateur();
		Claims body = parseClaimsJws.getBody();
		utilisateur.setNom(body.get("family_name", String.class));
		utilisateur.setPrenom(body.get("given_name", String.class));
		utilisateur.setEmail(body.get("email", String.class));
		utilisateur.setTimbre(body.get("timbre", String.class));
		utilisateur.setIdep(body.get("preferred_username", String.class));
		utilisateur.setMatricule(body.get("matricule", String.class));
		@SuppressWarnings("unchecked")
		List<String> roles = (List<String>) body.get("realm_access", LinkedHashMap.class).get("roles");
		utilisateur.setRoles(roles);
		return utilisateur;
	}
}
