package fr.insee.p7springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.insee.p7springboot.dao.LotsRepriseQDao;
import fr.insee.p7springboot.service.LotsRepriseQService;

@Service
public class LotsRepriseQServiceImpl implements LotsRepriseQService {

	@Autowired
	private LotsRepriseQDao lotsRepriseQDao;

	@Override
	public LotsRepriseQDao getDao() {
		return lotsRepriseQDao;
	}

}
