package fr.insee.p7springboot.service;

import fr.insee.p7springboot.dao.DrInseeDao;

public interface DrInseeService {

	public DrInseeDao getDao();

}
