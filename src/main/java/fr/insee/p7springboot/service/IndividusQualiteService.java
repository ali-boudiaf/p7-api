package fr.insee.p7springboot.service;


import fr.insee.p7springboot.dao.IndividusQualiteDao;

public interface IndividusQualiteService {

	public IndividusQualiteDao getDao();
}
