package fr.insee.p7springboot.service;

import fr.insee.p7springboot.dao.LotsRepriseQDao;

public interface LotsRepriseQService {

	public LotsRepriseQDao getDao();

}
