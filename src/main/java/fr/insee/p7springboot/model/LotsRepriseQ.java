package fr.insee.p7springboot.model;

import java.util.Set;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "lots_reprise_q", schema = "rp")
//@IdClass(LotsRepriseQPK.class)
public class LotsRepriseQ {

	@OneToMany(mappedBy = "lotIdQ")
	private Set<IndividusQualite> individusQualite;

	@Id
	@Column(name = "lot_id")
	private Integer lotId;

	@Column(name = "idep")
	private String idep;

	@Column(name = "lot_date")
	private Date lotDate;

	@Column(name = "lot_rang")
	private String lotRang;

	@Column(name = "lot_priorite")
	private String lotPriorite;

	@Column(name = "mode_traitement")
	private String modeTraitement;

	@Column(name = "lot_statut")
	private String lotStatut;

	@Column(name = "nb_bi")
	private Integer nbBi;

	@Column(name = "nb_bi_att")
	private Integer nbBiAtt;

	@Column(name = "nb_bi_rep_act")
	private Integer nbBiRepAct;

	@Column(name = "nb_bi_rep_act_att")
	private Integer nbBiRepActAtt;

	@Column(name = "nb_bi_rep_act_traite")
	private Integer nbBiRepActTraite;

	@Column(name = "nb_bi_traite")
	private Integer nbBiTraite;

	@Column(name = "dep_lot")
	private String depLot;

	@Column(name = "nb_bi_rep_prof")
	private Integer nbBiRepProf;

	@Column(name = "nb_bi_rep_prof_att")
	private Integer nbBiRepProfAtt;

	@Column(name = "nb_bi_rep_prof_traite")
	private Integer nbBiRepProfTraite;

	@Column(name = "vague_id")
	private Integer vagueId;

	@Column(name = "num_ls")
	private String numLs;

	@Column(name = "type_lot")
	private String typeLot;

	@Column(name = "origine_lot")
	private String origineLot;

	public Integer getLotId() {
		return lotId;
	}

	public void setLotId(Integer lotId) {
		this.lotId = lotId;
	}

	public String getIdep() {
		return idep;
	}

	public void setIdep(String idep) {
		this.idep = idep;
	}

	public Date getLotDate() {
		return lotDate;
	}

	public void setLotDate(Date lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotRang() {
		return lotRang;
	}

	public void setLotRang(String lotRang) {
		this.lotRang = lotRang;
	}

	public String getLotPriorite() {
		return lotPriorite;
	}

	public void setLotPriorite(String lotPriorite) {
		this.lotPriorite = lotPriorite;
	}

	public String getModeTraitement() {
		return modeTraitement;
	}

	public void setModeTraitement(String modeTraitement) {
		this.modeTraitement = modeTraitement;
	}

	public String getLotStatut() {
		return lotStatut;
	}

	public void setLotStatut(String lotStatut) {
		this.lotStatut = lotStatut;
	}

	public Integer getNbBi() {
		return nbBi;
	}

	public void setNbBi(Integer nbBi) {
		this.nbBi = nbBi;
	}

	public Integer getNbBiAtt() {
		return nbBiAtt;
	}

	public void setNbBiAtt(Integer nbBiAtt) {
		this.nbBiAtt = nbBiAtt;
	}

	public Integer getNbBiRepAct() {
		return nbBiRepAct;
	}

	public void setNbBiRepAct(Integer nbBiRepAct) {
		this.nbBiRepAct = nbBiRepAct;
	}

	public Integer getNbBiRepActAtt() {
		return nbBiRepActAtt;
	}

	public void setNbBiRepActAtt(Integer nbBiRepActAtt) {
		this.nbBiRepActAtt = nbBiRepActAtt;
	}

	public Integer getNbBiRepActTraite() {
		return nbBiRepActTraite;
	}

	public void setNbBiRepActTraite(Integer nbBiRepActTraite) {
		this.nbBiRepActTraite = nbBiRepActTraite;
	}

	public Integer getNbBiTraite() {
		return nbBiTraite;
	}

	public void setNbBiTraite(Integer nbBiTraite) {
		this.nbBiTraite = nbBiTraite;
	}

	public String getDepLot() {
		return depLot;
	}

	public void setDepLot(String depLot) {
		this.depLot = depLot;
	}

	public Integer getNbBiRepProf() {
		return nbBiRepProf;
	}

	public void setNbBiRepProf(Integer nbBiRepProf) {
		this.nbBiRepProf = nbBiRepProf;
	}

	public Integer getNbBiRepProfAtt() {
		return nbBiRepProfAtt;
	}

	public void setNbBiRepProfAtt(Integer nbBiRepProfAtt) {
		this.nbBiRepProfAtt = nbBiRepProfAtt;
	}

	public Integer getNbBiRepProfTraite() {
		return nbBiRepProfTraite;
	}

	public void setNbBiRepProfTraite(Integer nbBiRepProfTraite) {
		this.nbBiRepProfTraite = nbBiRepProfTraite;
	}

	public Integer getVagueId() {
		return vagueId;
	}

	public void setVagueId(Integer vagueId) {
		this.vagueId = vagueId;
	}

	public String getNumLs() {
		return numLs;
	}

	public void setNumLs(String numLs) {
		this.numLs = numLs;
	}

	public String getTypeLot() {
		return typeLot;
	}

	public void setTypeLot(String typeLot) {
		this.typeLot = typeLot;
	}

	public String getOrigineLot() {
		return origineLot;
	}

	public void setOrigineLot(String origineLot) {
		this.origineLot = origineLot;
	}

	public Set<IndividusQualite> getIndividusQualite() {
		return individusQualite;
	}

	public void setIndividusQualite(Set<IndividusQualite> individusQualite) {
		this.individusQualite = individusQualite;
	}
}
