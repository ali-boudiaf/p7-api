package fr.insee.p7springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dr_insee", schema = "rp")
public class DrInsee {

	@Id
	@Column(name = "dr_code")
	private String drCode;

	@Column(name = "nom")
	private String nom;

	@Column(name = "dr_num_voie")
	private String drNumVoie;

	@Column(name = "dr_bis_ter")
	private String drBisTer;

	@Column(name = "dr_type_voie")
	private String drTypeVoie;

	@Column(name = "dr_lib_voie")
	private String drLibVoie;

	@Column(name = "dr_complement")
	private String drComplement;

	@Column(name = "dr_code_postal")
	private String drCodePostal;

	@Column(name = "dr_nom_commune")
	private String drNomCommune;

	@Column(name = "dr_cedex")
	private String drCedex;

	@Column(name = "dr_boite_postale")
	private String drBoitePostale;

	@Column(name = "liv_nom")
	private String livNom;

	@Column(name = "liv_num_voie")
	private String livNumVoie;

	@Column(name = "liv_bis_ter")
	private String livBisTer;

	@Column(name = "liv_type_voie")
	private String livTypeVoie;

	@Column(name = "liv_lib_voie")
	private String livLibVoie;

	@Column(name = "liv_complement")
	private String livComplement;

	@Column(name = "liv_code_postal")
	private String livCodePostal;

	@Column(name = "liv_nom_commune")
	private String livNomCommune;

	@Column(name = "liv_cedex")
	private String livCedex;

	@Column(name = "liv_boite_postale")
	private String livBoitePostale;

	@Column(name = "liv_conditions")
	private String livConditions;

	@Column(name = "civilite_dir")
	private String civiliteDir;

	@Column(name = "nom_dir")
	private String nomDir;

	@Column(name = "prenom_dir")
	private String prenomDir;

	@Column(name = "tel_standard")
	private String telStandard;

	@Column(name = "tel_rrp")
	private String telRrp;

	@Column(name = "fax_rrp")
	private String faxRrp;

	@Column(name = "tel_accueil_national")
	private String telAccueilNational;

	@Column(name = "nb_sup_prv")
	private Integer nbSupPrv;

	@Column(name = "nb_sup_ext_prv")
	private Integer nbSupExtPrv;

	@Column(name = "nb_ecmt_prv")
	private Integer nbEcmtPrv;

	@Column(name = "nb_ectl_prv")
	private Integer nbEctlPrv;

	@Column(name = "nb_ecarto_prv")
	private Integer nbEcartoPrv;

	@Column(name = "nb_sup_rea")
	private Integer nbSupRea;

	@Column(name = "nb_sup_ext_rea")
	private Integer nbSupExtRea;

	@Column(name = "nb_ecmt_rea")
	private Integer nbEcmtRea;

	@Column(name = "nb_ectl_rea")
	private Integer nbEctlRea;

	@Column(name = "nb_ecarto_rea")
	private Integer nbEcartoRea;

	@Column(name = "nb_ut_prev")
	private Integer nbUtPrev;

	@Column(name = "ind_fin_ut")
	private String indFinUt;

	@Column(name = "tx_qualite_actet")
	private Integer txQualiteActet;

	@Column(name = "tx_qualite_profant")
	private Integer txQualiteProfant;

	@Column(name = "tx_qualite_prof")
	private Integer txQualiteProf;

	@Column(name = "tx_arbitrage_actet")
	private Integer txArbitrageActet;

	@Column(name = "tx_arbitrage_profant")
	private Integer txArbitrageProfant;

	@Column(name = "tx_arbitrage_prof")
	private Integer txArbitrageProf;

	@Column(name = "sav_nom")
	private String savNom;

	@Column(name = "sav_prenom")
	private String savPrenom;

	@Column(name = "sav_tel")
	private String savTel;

	@Column(name = "sav_fax")
	private String savFax;

	@Column(name = "sav_mail")
	private String savMail;

	@Column(name = "civilite_resp_rp")
	private String civiliteRespRp;

	@Column(name = "nom_resp_rp")
	private String nomRespRp;

	@Column(name = "prenom_resp_rp")
	private String prenomRespRp;

	@Column(name = "tel_resp_rp")
	private String telRespRp;

	@Column(name = "email_resp_rp")
	private String emailRespRp;

	// 4 bytes variable-precision, inexact 6 decimal digits precision
	@Column(name = "tx_qualite_siret")
	private Integer txQualiteSiret;

	public String getDrCode() {
		return drCode;
	}

	public void setDrCode(String drCode) {
		this.drCode = drCode;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDrNumVoie() {
		return drNumVoie;
	}

	public void setDrNumVoie(String drNumVoie) {
		this.drNumVoie = drNumVoie;
	}

	public String getDrBisTer() {
		return drBisTer;
	}

	public void setDrBisTer(String drBisTer) {
		this.drBisTer = drBisTer;
	}

	public String getDrTypeVoie() {
		return drTypeVoie;
	}

	public void setDrTypeVoie(String drTypeVoie) {
		this.drTypeVoie = drTypeVoie;
	}

	public String getDrLibVoie() {
		return drLibVoie;
	}

	public void setDrLibVoie(String drLibVoie) {
		this.drLibVoie = drLibVoie;
	}

	public String getDrComplement() {
		return drComplement;
	}

	public void setDrComplement(String drComplement) {
		this.drComplement = drComplement;
	}

	public String getDrCodePostal() {
		return drCodePostal;
	}

	public void setDrCodePostal(String drCodePostal) {
		this.drCodePostal = drCodePostal;
	}

	public String getDrNomCommune() {
		return drNomCommune;
	}

	public void setDrNomCommune(String drNomCommune) {
		this.drNomCommune = drNomCommune;
	}

	public String getDrCedex() {
		return drCedex;
	}

	public void setDrCedex(String drCedex) {
		this.drCedex = drCedex;
	}

	public String getDrBoitePostale() {
		return drBoitePostale;
	}

	public void setDrBoitePostale(String drBoitePostale) {
		this.drBoitePostale = drBoitePostale;
	}

	public String getLivNom() {
		return livNom;
	}

	public void setLivNom(String livNom) {
		this.livNom = livNom;
	}

	public String getLivNumVoie() {
		return livNumVoie;
	}

	public void setLivNumVoie(String livNumVoie) {
		this.livNumVoie = livNumVoie;
	}

	public String getLivBisTer() {
		return livBisTer;
	}

	public void setLivBisTer(String livBisTer) {
		this.livBisTer = livBisTer;
	}

	public String getLivTypeVoie() {
		return livTypeVoie;
	}

	public void setLivTypeVoie(String livTypeVoie) {
		this.livTypeVoie = livTypeVoie;
	}

	public String getLivLibVoie() {
		return livLibVoie;
	}

	public void setLivLibVoie(String livLibVoie) {
		this.livLibVoie = livLibVoie;
	}

	public String getLivComplement() {
		return livComplement;
	}

	public void setLivComplement(String livComplement) {
		this.livComplement = livComplement;
	}

	public String getLivCodePostal() {
		return livCodePostal;
	}

	public void setLivCodePostal(String livCodePostal) {
		this.livCodePostal = livCodePostal;
	}

	public String getLivNomCommune() {
		return livNomCommune;
	}

	public void setLivNomCommune(String livNomCommune) {
		this.livNomCommune = livNomCommune;
	}

	public String getLivCedex() {
		return livCedex;
	}

	public void setLivCedex(String livCedex) {
		this.livCedex = livCedex;
	}

	public String getLivBoitePostale() {
		return livBoitePostale;
	}

	public void setLivBoitePostale(String livBoitePostale) {
		this.livBoitePostale = livBoitePostale;
	}

	public String getLivConditions() {
		return livConditions;
	}

	public void setLivConditions(String livConditions) {
		this.livConditions = livConditions;
	}

	public String getCiviliteDir() {
		return civiliteDir;
	}

	public void setCiviliteDir(String civiliteDir) {
		this.civiliteDir = civiliteDir;
	}

	public String getNomDir() {
		return nomDir;
	}

	public void setNomDir(String nomDir) {
		this.nomDir = nomDir;
	}

	public String getPrenomDir() {
		return prenomDir;
	}

	public void setPrenomDir(String prenomDir) {
		this.prenomDir = prenomDir;
	}

	public String getTelStandard() {
		return telStandard;
	}

	public void setTelStandard(String telStandard) {
		this.telStandard = telStandard;
	}

	public String getTelRrp() {
		return telRrp;
	}

	public void setTelRrp(String telRrp) {
		this.telRrp = telRrp;
	}

	public String getFaxRrp() {
		return faxRrp;
	}

	public void setFaxRrp(String faxRrp) {
		this.faxRrp = faxRrp;
	}

	public String getTelAccueilNational() {
		return telAccueilNational;
	}

	public void setTelAccueilNational(String telAccueilNational) {
		this.telAccueilNational = telAccueilNational;
	}

	public Integer getNbSupPrv() {
		return nbSupPrv;
	}

	public void setNbSupPrv(Integer nbSupPrv) {
		this.nbSupPrv = nbSupPrv;
	}

	public Integer getNbSupExtPrv() {
		return nbSupExtPrv;
	}

	public void setNbSupExtPrv(Integer nbSupExtPrv) {
		this.nbSupExtPrv = nbSupExtPrv;
	}

	public Integer getNbEcmtPrv() {
		return nbEcmtPrv;
	}

	public void setNbEcmtPrv(Integer nbEcmtPrv) {
		this.nbEcmtPrv = nbEcmtPrv;
	}

	public Integer getNbEctlPrv() {
		return nbEctlPrv;
	}

	public void setNbEctlPrv(Integer nbEctlPrv) {
		this.nbEctlPrv = nbEctlPrv;
	}

	public Integer getNbEcartoPrv() {
		return nbEcartoPrv;
	}

	public void setNbEcartoPrv(Integer nbEcartoPrv) {
		this.nbEcartoPrv = nbEcartoPrv;
	}

	public Integer getNbSupRea() {
		return nbSupRea;
	}

	public void setNbSupRea(Integer nbSupRea) {
		this.nbSupRea = nbSupRea;
	}

	public Integer getNbSupExtRea() {
		return nbSupExtRea;
	}

	public void setNbSupExtRea(Integer nbSupExtRea) {
		this.nbSupExtRea = nbSupExtRea;
	}

	public Integer getNbEcmtRea() {
		return nbEcmtRea;
	}

	public void setNbEcmtRea(Integer nbEcmtRea) {
		this.nbEcmtRea = nbEcmtRea;
	}

	public Integer getNbEctlRea() {
		return nbEctlRea;
	}

	public void setNbEctlRea(Integer nbEctlRea) {
		this.nbEctlRea = nbEctlRea;
	}

	public Integer getNbEcartoRea() {
		return nbEcartoRea;
	}

	public void setNbEcartoRea(Integer nbEcartoRea) {
		this.nbEcartoRea = nbEcartoRea;
	}

	public Integer getNbUtPrev() {
		return nbUtPrev;
	}

	public void setNbUtPrev(Integer nbUtPrev) {
		this.nbUtPrev = nbUtPrev;
	}

	public String getIndFinUt() {
		return indFinUt;
	}

	public void setIndFinUt(String indFinUt) {
		this.indFinUt = indFinUt;
	}

	public Integer getTxQualiteActet() {
		return txQualiteActet;
	}

	public void setTxQualiteActet(Integer txQualiteActet) {
		this.txQualiteActet = txQualiteActet;
	}

	public Integer getTxQualiteProfant() {
		return txQualiteProfant;
	}

	public void setTxQualiteProfant(Integer txQualiteProfant) {
		this.txQualiteProfant = txQualiteProfant;
	}

	public Integer getTxQualiteProf() {
		return txQualiteProf;
	}

	public void setTxQualiteProf(Integer txQualiteProf) {
		this.txQualiteProf = txQualiteProf;
	}

	public Integer getTxArbitrageActet() {
		return txArbitrageActet;
	}

	public void setTxArbitrageActet(Integer txArbitrageActet) {
		this.txArbitrageActet = txArbitrageActet;
	}

	public Integer getTxArbitrageProfant() {
		return txArbitrageProfant;
	}

	public void setTxArbitrageProfant(Integer txArbitrageProfant) {
		this.txArbitrageProfant = txArbitrageProfant;
	}

	public Integer getTxArbitrageProf() {
		return txArbitrageProf;
	}

	public void setTxArbitrageProf(Integer txArbitrageProf) {
		this.txArbitrageProf = txArbitrageProf;
	}

	public String getSavNom() {
		return savNom;
	}

	public void setSavNom(String savNom) {
		this.savNom = savNom;
	}

	public String getSavPrenom() {
		return savPrenom;
	}

	public void setSavPrenom(String savPrenom) {
		this.savPrenom = savPrenom;
	}

	public String getSavTel() {
		return savTel;
	}

	public void setSavTel(String savTel) {
		this.savTel = savTel;
	}

	public String getSavFax() {
		return savFax;
	}

	public void setSavFax(String savFax) {
		this.savFax = savFax;
	}

	public String getSavMail() {
		return savMail;
	}

	public void setSavMail(String savMail) {
		this.savMail = savMail;
	}

	public String getCiviliteRespRp() {
		return civiliteRespRp;
	}

	public void setCiviliteRespRp(String civiliteRespRp) {
		this.civiliteRespRp = civiliteRespRp;
	}

	public String getNomRespRp() {
		return nomRespRp;
	}

	public void setNomRespRp(String nomRespRp) {
		this.nomRespRp = nomRespRp;
	}

	public String getPrenomRespRp() {
		return prenomRespRp;
	}

	public void setPrenomRespRp(String prenomRespRp) {
		this.prenomRespRp = prenomRespRp;
	}

	public String getTelRespRp() {
		return telRespRp;
	}

	public void setTelRespRp(String telRespRp) {
		this.telRespRp = telRespRp;
	}

	public String getEmailRespRp() {
		return emailRespRp;
	}

	public void setEmailRespRp(String emailRespRp) {
		this.emailRespRp = emailRespRp;
	}

	public Integer getTxQualiteSiret() {
		return txQualiteSiret;
	}

	public void setTxQualiteSiret(Integer txQualiteSiret) {
		this.txQualiteSiret = txQualiteSiret;
	}

}
