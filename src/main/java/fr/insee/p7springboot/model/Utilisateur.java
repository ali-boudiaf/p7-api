package fr.insee.p7springboot.model;

import java.io.Serializable;
import java.util.List;

public class Utilisateur implements Serializable {

	private static final long serialVersionUID = 3482687919603159024L;

	private String nom;
	private String prenom;
	private String email;
	private String idep;
	private String matricule;
	private String timbre;
	private List<String> roles;

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getEmail() {
		return email;
	}

	public String getIdep() {
		return idep;
	}

	public String getMatricule() {
		return matricule;
	}

	public String getTimbre() {
		return timbre;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setIdep(String idep) {
		this.idep = idep;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public void setTimbre(String timbre) {
		this.timbre = timbre;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Utilisateur [nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", idep=" + idep + ", matricule=" + matricule + ", timbre=" + timbre + ", roles=" + roles + "]";
	}

}
