package fr.insee.p7springboot.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class IndividusQualitePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7147950039610752043L;

	public IndividusQualitePK() {
	}

	public IndividusQualitePK(String depCode, String comCode, String irisIlot, int adrRang, int logRang, int indRang,
			String procesCodageInit) {
		super();
		this.depCode = depCode;
		this.comCode = comCode;
		this.irisIlot = irisIlot;
		this.adrRang = adrRang;
		this.logRang = logRang;
		this.indRang = indRang;
		this.procesCodageInit = procesCodageInit;
	}

	private String depCode;

	private String comCode;

	private String irisIlot;

	private int adrRang;

	private int logRang;

	private int indRang;

	private String procesCodageInit;

	public String getDepCode() {
		return depCode;
	}

	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}

	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	public String getIrisIlot() {
		return irisIlot;
	}

	public void setIrisIlot(String irisIlot) {
		this.irisIlot = irisIlot;
	}

	public int getAdrRang() {
		return adrRang;
	}

	public void setAdrRang(int adrRang) {
		this.adrRang = adrRang;
	}

	public int getLogRang() {
		return logRang;
	}

	public void setLogRang(int logRang) {
		this.logRang = logRang;
	}

	public int getIndRang() {
		return indRang;
	}

	public void setIndRang(int indRang) {
		this.indRang = indRang;
	}

	public String getProcesCodageInit() {
		return procesCodageInit;
	}

	public void setProcesCodageInit(String procesCodageInit) {
		this.procesCodageInit = procesCodageInit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + adrRang;
		result = prime * result + ((comCode == null) ? 0 : comCode.hashCode());
		result = prime * result + ((depCode == null) ? 0 : depCode.hashCode());
		result = prime * result + indRang;
		result = prime * result + ((irisIlot == null) ? 0 : irisIlot.hashCode());
		result = prime * result + logRang;
		result = prime * result + ((procesCodageInit == null) ? 0 : procesCodageInit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndividusQualitePK other = (IndividusQualitePK) obj;
		if (adrRang != other.adrRang)
			return false;
		if (comCode == null) {
			if (other.comCode != null)
				return false;
		} else if (!comCode.equals(other.comCode))
			return false;
		if (depCode == null) {
			if (other.depCode != null)
				return false;
		} else if (!depCode.equals(other.depCode))
			return false;
		if (indRang != other.indRang)
			return false;
		if (irisIlot == null) {
			if (other.irisIlot != null)
				return false;
		} else if (!irisIlot.equals(other.irisIlot))
			return false;
		if (logRang != other.logRang)
			return false;
		if (procesCodageInit == null) {
			if (other.procesCodageInit != null)
				return false;
		} else if (!procesCodageInit.equals(other.procesCodageInit))
			return false;
		return true;
	}

}
