package fr.insee.p7springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import fr.insee.p7springboot.model.pk.IndividusQualitePK;

@Entity
@Table(name = "individus_qualite", schema = "rp")
@IdClass(IndividusQualitePK.class)
public class IndividusQualite {

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "lot_id_q", insertable = false, updatable = false)
	private LotsRepriseQ lotsRepriseQ;

	@Id
	@Column(name = "dep_code")
	private String depCode;

	@Id
	@Column(name = "com_code")
	private String comCode;

	@Id
	@Column(name = "iris_ilot")
	private String irisIlot;

	@Id
	@Column(name = "adr_rang")
	private Integer adrRang;

	@Id
	@Column(name = "log_rang")
	private Integer logRang;

	@Id
	@Column(name = "ind_rang")
	private Integer indRang;

	@Id
	@Column(name = "proces_codage_init")
	private String procesCodageInit;

	@Column(name = "depcom_code")
	private String depcomCode;
	@Column(name = "petite_grande")
	private String petiteGrande;
	@Column(name = "cil")
	private String cil;
	@Column(name = "fil")
	private String fil;

	@Column(name = "enr_ind")
	private String enrInd;
	@Column(name = "cabbi")
	private String cabbi;
	@Column(name = "cabfl")
	private String cabfl;
	@Column(name = "cabprov")
	private String cabprov;
	@Column(name = "compl_x")
	private String complX;
	@Column(name = "edp_x")
	private String edpX;
	@Column(name = "cmt_id")
	private String cmtId;
	@Column(name = "num_ls")
	private String numLs;
	@Column(name = "lot_id")
	private Integer lotId;
	@Column(name = "dr_code")
	private String drCode;
	@Column(name = "idep")
	private String idep;
	@Column(name = "lot_id_q")
	private Integer lotIdQ;

	@Column(name = "sexe_x")
	private String sexeX;
	@Column(name = "jnai_x")
	private Integer jnaiX;
	@Column(name = "mnai_x")
	private Integer mnaiX;
	@Column(name = "anai_x")
	private Integer anaiX;
	@Column(name = "dipl_x")
	private String diplX;
	@Column(name = "i_etat_individu")
	private String iEtatIndividu;
	@Column(name = "arbitrage")
	private String arbitrage;
	@Column(name = "plt_x")
	private String pltX;
	@Column(name = "dlt_x")
	private String dltX;
	@Column(name = "clt_x")
	private String cltX;
	@Column(name = "ilt_x")
	private String iltX;
	@Column(name = "vardompart_x")
	private String vardompartX;
	@Column(name = "rs_x")
	private String rsX;
	@Column(name = "typevoi_x")
	private String typevoiX;
	@Column(name = "numvoi_x")
	private String numvoiX;
	@Column(name = "bister_x")
	private String bisterX;
	@Column(name = "nomvoi_x")
	private String nomvoiX;
	@Column(name = "cpladr_x")
	private String cpladrX;
	@Column(name = "i_clt_dec")
	private String iCltDec;
	@Column(name = "clt_c")
	private String cltC;
	@Column(name = "clt_m_q")
	private String cltMQ;
	@Column(name = "clt_c_m_q")
	private String cltCMQ;
	@Column(name = "clt_c_m_arb")
	private String cltCMArb;
	@Column(name = "i_actet_dec")
	private String iActetDec;
	@Column(name = "actet_c")
	private String actetC;
	@Column(name = "actet_c_m_q")
	private String actetCMQ;
	@Column(name = "actet_c_m_arb")
	private String actetCMArb;
	@Column(name = "siret_dec")
	private String siretDec;
	@Column(name = "siretq")
	private String siretq;
	@Column(name = "siret_arb")
	private String siretArb;
	@Column(name = "typevoi_mq")
	private String typevoiMq;
	@Column(name = "numvoi_mq")
	private String numvoiMq;
	@Column(name = "bister_mq")
	private String bisterMq;
	@Column(name = "nomvoi_mq")
	private String nomvoiMq;
	@Column(name = "rs_mq")
	private String rsMq;
	@Column(name = "actet_x")
	private String actetX;
	@Column(name = "actet_l_m_q")
	private String actetLMQ;
	@Column(name = "inrs_mca")
	private String inrsMca;
	@Column(name = "note_mca_nom_dec")
	private String noteMcaNomDec;
	@Column(name = "note_mca_adr_dec")
	private String noteMcaAdrDec;
	@Column(name = "classe_dec")
	private String classeDec;
	@Column(name = "i_mca_dec")
	private String iMcaDec;
	@Column(name = "inrs_mca_q")
	private String inrsMcaQ;
	@Column(name = "note_mca_nom_q")
	private String noteMcaNomQ;
	@Column(name = "note_mca_adr_q")
	private String noteMcaAdrQ;
	@Column(name = "classe_q")
	private String classeQ;
	@Column(name = "i_mca_q")
	private String iMcaQ;
	@Column(name = "inrs_mca_arb")
	private String inrsMcaArb;
	@Column(name = "note_mca_nom_arb")
	private String noteMcaNomArb;
	@Column(name = "note_mca_adr_arb")
	private String noteMcaAdrArb;
	@Column(name = "classe_arb")
	private String classeArb;
	@Column(name = "i_mca_arb")
	private String iMcaArb;
	@Column(name = "i_reprise_act_q")
	private String iRepriseActQ;
	@Column(name = "situat_x")
	private String situatX;
	@Column(name = "statav_x")
	private String statavX;
	@Column(name = "tp_x")
	private String tpX;
	@Column(name = "stat_x")
	private String statX;
	@Column(name = "nbsal_x")
	private String nbsalX;
	@Column(name = "empl_x")
	private String emplX;
	@Column(name = "posp_x")
	private String pospX;
	@Column(name = "fonc_x")
	private String foncX;
	@Column(name = "i_reprise_prof_q")
	private String iRepriseProfQ;
	@Column(name = "profa_x")
	private String profaX;
	@Column(name = "profa_l_m")
	private String profaLM;
	@Column(name = "profa_c")
	private String profaC;
	@Column(name = "profa_l_m_q")
	private String profaLMQ;
	@Column(name = "profa_c_m_q")
	private String profaCMQ;
	@Column(name = "profa_l_m_arb")
	private String profaLMArb;
	@Column(name = "profa_c_m_arb")
	private String profaCMArb;
	@Column(name = "profi_x")
	private String profiX;
	@Column(name = "profi_l_m")
	private String profiLM;
	@Column(name = "profi_c")
	private String profiC;
	@Column(name = "profi_l_m_q")
	private String profiLMQ;
	@Column(name = "profi_c_m_q")
	private String profiCMQ;
	@Column(name = "profi_l_m_arb")
	private String profiLMArb;
	@Column(name = "profi_c_m_arb")
	private String profiCMArb;
	@Column(name = "profs_x")
	private String profsX;
	@Column(name = "profs_l_m")
	private String profsLM;
	@Column(name = "profs_c")
	private String profsC;
	@Column(name = "profs_l_m_q")
	private String profsLMQ;
	@Column(name = "profs_c_m_q")
	private String profsCMQ;
	@Column(name = "profs_l_m_arb")
	private String profsLMArb;
	@Column(name = "profs_c_m_arb")
	private String profsCMArb;
	@Column(name = "i_clt_m_q")
	private String iCltMQ;
	@Column(name = "dlt_c_m_q")
	private String dltCMQ;
	@Column(name = "i_actet_q")
	private String iActetQ;
	@Column(name = "i_siret_q")
	private String iSiretQ;
	@Column(name = "i_profs_q")
	private String iProfsQ;
	@Column(name = "i_profi_q")
	private String iProfiQ;
	@Column(name = "i_profa_q")
	private String iProfaQ;
	@Column(name = "dr_code_recap")
	private String drCodeRecap;
	@Column(name = "idep_recap")
	private String idepRecap;
	@Column(name = "poids_bi")
	private Integer poidsBi;
	@Column(name = "clt_c_c")
	private String cltCC;
	@Column(name = "i_clt_c")
	private String iCltC;
	@Column(name = "dlt_c_c")
	private String dltCC;
	@Column(name = "commentaire_activite")
	private String commentaireActivite;
	@Column(name = "lot_id_q_sauv")
	private Integer lotIdQSauv;

	public String getDepCode() {
		return depCode;
	}

	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}

	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	public String getDepcomCode() {
		return depcomCode;
	}

	public void setDepcomCode(String depcomCode) {
		this.depcomCode = depcomCode;
	}

	public String getPetiteGrande() {
		return petiteGrande;
	}

	public void setPetiteGrande(String petiteGrande) {
		this.petiteGrande = petiteGrande;
	}

	public String getCil() {
		return cil;
	}

	public void setCil(String cil) {
		this.cil = cil;
	}

	public String getIrisIlot() {
		return irisIlot;
	}

	public void setIrisIlot(String irisIlot) {
		this.irisIlot = irisIlot;
	}

	public String getFil() {
		return fil;
	}

	public void setFil(String fil) {
		this.fil = fil;
	}

	public Integer getAdrRang() {
		return adrRang;
	}

	public void setAdrRang(Integer adrRang) {
		this.adrRang = adrRang;
	}

	public Integer getLogRang() {
		return logRang;
	}

	public void setLogRang(Integer logRang) {
		this.logRang = logRang;
	}

	public Integer getIndRang() {
		return indRang;
	}

	public void setIndRang(Integer indRang) {
		this.indRang = indRang;
	}

	public String getEnrInd() {
		return enrInd;
	}

	public void setEnrInd(String enrInd) {
		this.enrInd = enrInd;
	}

	public String getCabbi() {
		return cabbi;
	}

	public void setCabbi(String cabbi) {
		this.cabbi = cabbi;
	}

	public String getCabfl() {
		return cabfl;
	}

	public void setCabfl(String cabfl) {
		this.cabfl = cabfl;
	}

	public String getCabprov() {
		return cabprov;
	}

	public void setCabprov(String cabprov) {
		this.cabprov = cabprov;
	}

	public String getComplX() {
		return complX;
	}

	public void setComplX(String complX) {
		this.complX = complX;
	}

	public String getEdpX() {
		return edpX;
	}

	public void setEdpX(String edpX) {
		this.edpX = edpX;
	}

	public String getCmtId() {
		return cmtId;
	}

	public void setCmtId(String cmtId) {
		this.cmtId = cmtId;
	}

	public String getNumLs() {
		return numLs;
	}

	public void setNumLs(String numLs) {
		this.numLs = numLs;
	}

	public Integer getLotId() {
		return lotId;
	}

	public void setLotId(Integer lotId) {
		this.lotId = lotId;
	}

	public String getDrCode() {
		return drCode;
	}

	public void setDrCode(String drCode) {
		this.drCode = drCode;
	}

	public String getIdep() {
		return idep;
	}

	public void setIdep(String idep) {
		this.idep = idep;
	}

	public Integer getLotIdQ() {
		return lotIdQ;
	}

	public void setLotIdQ(Integer lotIdQ) {
		this.lotIdQ = lotIdQ;
	}

	public String getProcesCodageInit() {
		return procesCodageInit;
	}

	public void setProcesCodageInit(String procesCodageInit) {
		this.procesCodageInit = procesCodageInit;
	}

	public String getSexeX() {
		return sexeX;
	}

	public void setSexeX(String sexeX) {
		this.sexeX = sexeX;
	}

	public Integer getJnaiX() {
		return jnaiX;
	}

	public void setJnaiX(Integer jnaiX) {
		this.jnaiX = jnaiX;
	}

	public Integer getMnaiX() {
		return mnaiX;
	}

	public void setMnaiX(Integer mnaiX) {
		this.mnaiX = mnaiX;
	}

	public Integer getAnaiX() {
		return anaiX;
	}

	public void setAnaiX(Integer anaiX) {
		this.anaiX = anaiX;
	}

	public String getDiplX() {
		return diplX;
	}

	public void setDiplX(String diplX) {
		this.diplX = diplX;
	}

	public String getiEtatIndividu() {
		return iEtatIndividu;
	}

	public void setiEtatIndividu(String iEtatIndividu) {
		this.iEtatIndividu = iEtatIndividu;
	}

	public String getArbitrage() {
		return arbitrage;
	}

	public void setArbitrage(String arbitrage) {
		this.arbitrage = arbitrage;
	}

	public String getPltX() {
		return pltX;
	}

	public void setPltX(String pltX) {
		this.pltX = pltX;
	}

	public String getDltX() {
		return dltX;
	}

	public void setDltX(String dltX) {
		this.dltX = dltX;
	}

	public String getCltX() {
		return cltX;
	}

	public void setCltX(String cltX) {
		this.cltX = cltX;
	}

	public String getIltX() {
		return iltX;
	}

	public void setIltX(String iltX) {
		this.iltX = iltX;
	}

	public String getVardompartX() {
		return vardompartX;
	}

	public void setVardompartX(String vardompartX) {
		this.vardompartX = vardompartX;
	}

	public String getRsX() {
		return rsX;
	}

	public void setRsX(String rsX) {
		this.rsX = rsX;
	}

	public String getTypevoiX() {
		return typevoiX;
	}

	public void setTypevoiX(String typevoiX) {
		this.typevoiX = typevoiX;
	}

	public String getNumvoiX() {
		return numvoiX;
	}

	public void setNumvoiX(String numvoiX) {
		this.numvoiX = numvoiX;
	}

	public String getBisterX() {
		return bisterX;
	}

	public void setBisterX(String bisterX) {
		this.bisterX = bisterX;
	}

	public String getNomvoiX() {
		return nomvoiX;
	}

	public void setNomvoiX(String nomvoiX) {
		this.nomvoiX = nomvoiX;
	}

	public String getCpladrX() {
		return cpladrX;
	}

	public void setCpladrX(String cpladrX) {
		this.cpladrX = cpladrX;
	}

	public String getiCltDec() {
		return iCltDec;
	}

	public void setiCltDec(String iCltDec) {
		this.iCltDec = iCltDec;
	}

	public String getCltC() {
		return cltC;
	}

	public void setCltC(String cltC) {
		this.cltC = cltC;
	}

	public String getCltMQ() {
		return cltMQ;
	}

	public void setCltMQ(String cltMQ) {
		this.cltMQ = cltMQ;
	}

	public String getCltCMQ() {
		return cltCMQ;
	}

	public void setCltCMQ(String cltCMQ) {
		this.cltCMQ = cltCMQ;
	}

	public String getCltCMArb() {
		return cltCMArb;
	}

	public void setCltCMArb(String cltCMArb) {
		this.cltCMArb = cltCMArb;
	}

	public String getiActetDec() {
		return iActetDec;
	}

	public void setiActetDec(String iActetDec) {
		this.iActetDec = iActetDec;
	}

	public String getActetC() {
		return actetC;
	}

	public void setActetC(String actetC) {
		this.actetC = actetC;
	}

	public String getActetCMQ() {
		return actetCMQ;
	}

	public void setActetCMQ(String actetCMQ) {
		this.actetCMQ = actetCMQ;
	}

	public String getActetCMArb() {
		return actetCMArb;
	}

	public void setActetCMArb(String actetCMArb) {
		this.actetCMArb = actetCMArb;
	}

	public String getSiretDec() {
		return siretDec;
	}

	public void setSiretDec(String siretDec) {
		this.siretDec = siretDec;
	}

	public String getSiretq() {
		return siretq;
	}

	public void setSiretq(String siretq) {
		this.siretq = siretq;
	}

	public String getSiretArb() {
		return siretArb;
	}

	public void setSiretArb(String siretArb) {
		this.siretArb = siretArb;
	}

	public String getTypevoiMq() {
		return typevoiMq;
	}

	public void setTypevoiMq(String typevoiMq) {
		this.typevoiMq = typevoiMq;
	}

	public String getNumvoiMq() {
		return numvoiMq;
	}

	public void setNumvoiMq(String numvoiMq) {
		this.numvoiMq = numvoiMq;
	}

	public String getBisterMq() {
		return bisterMq;
	}

	public void setBisterMq(String bisterMq) {
		this.bisterMq = bisterMq;
	}

	public String getNomvoiMq() {
		return nomvoiMq;
	}

	public void setNomvoiMq(String nomvoiMq) {
		this.nomvoiMq = nomvoiMq;
	}

	public String getRsMq() {
		return rsMq;
	}

	public void setRsMq(String rsMq) {
		this.rsMq = rsMq;
	}

	public String getActetX() {
		return actetX;
	}

	public void setActetX(String actetX) {
		this.actetX = actetX;
	}

	public String getActetLMQ() {
		return actetLMQ;
	}

	public void setActetLMQ(String actetLMQ) {
		this.actetLMQ = actetLMQ;
	}

	public String getInrsMca() {
		return inrsMca;
	}

	public void setInrsMca(String inrsMca) {
		this.inrsMca = inrsMca;
	}

	public String getNoteMcaNomDec() {
		return noteMcaNomDec;
	}

	public void setNoteMcaNomDec(String noteMcaNomDec) {
		this.noteMcaNomDec = noteMcaNomDec;
	}

	public String getNoteMcaAdrDec() {
		return noteMcaAdrDec;
	}

	public void setNoteMcaAdrDec(String noteMcaAdrDec) {
		this.noteMcaAdrDec = noteMcaAdrDec;
	}

	public String getClasseDec() {
		return classeDec;
	}

	public void setClasseDec(String classeDec) {
		this.classeDec = classeDec;
	}

	public String getiMcaDec() {
		return iMcaDec;
	}

	public void setiMcaDec(String iMcaDec) {
		this.iMcaDec = iMcaDec;
	}

	public String getInrsMcaQ() {
		return inrsMcaQ;
	}

	public void setInrsMcaQ(String inrsMcaQ) {
		this.inrsMcaQ = inrsMcaQ;
	}

	public String getNoteMcaNomQ() {
		return noteMcaNomQ;
	}

	public void setNoteMcaNomQ(String noteMcaNomQ) {
		this.noteMcaNomQ = noteMcaNomQ;
	}

	public String getNoteMcaAdrQ() {
		return noteMcaAdrQ;
	}

	public void setNoteMcaAdrQ(String noteMcaAdrQ) {
		this.noteMcaAdrQ = noteMcaAdrQ;
	}

	public String getClasseQ() {
		return classeQ;
	}

	public void setClasseQ(String classeQ) {
		this.classeQ = classeQ;
	}

	public String getiMcaQ() {
		return iMcaQ;
	}

	public void setiMcaQ(String iMcaQ) {
		this.iMcaQ = iMcaQ;
	}

	public String getInrsMcaArb() {
		return inrsMcaArb;
	}

	public void setInrsMcaArb(String inrsMcaArb) {
		this.inrsMcaArb = inrsMcaArb;
	}

	public String getNoteMcaNomArb() {
		return noteMcaNomArb;
	}

	public void setNoteMcaNomArb(String noteMcaNomArb) {
		this.noteMcaNomArb = noteMcaNomArb;
	}

	public String getNoteMcaAdrArb() {
		return noteMcaAdrArb;
	}

	public void setNoteMcaAdrArb(String noteMcaAdrArb) {
		this.noteMcaAdrArb = noteMcaAdrArb;
	}

	public String getClasseArb() {
		return classeArb;
	}

	public void setClasseArb(String classeArb) {
		this.classeArb = classeArb;
	}

	public String getiMcaArb() {
		return iMcaArb;
	}

	public void setiMcaArb(String iMcaArb) {
		this.iMcaArb = iMcaArb;
	}

	public String getiRepriseActQ() {
		return iRepriseActQ;
	}

	public void setiRepriseActQ(String iRepriseActQ) {
		this.iRepriseActQ = iRepriseActQ;
	}

	public String getSituatX() {
		return situatX;
	}

	public void setSituatX(String situatX) {
		this.situatX = situatX;
	}

	public String getStatavX() {
		return statavX;
	}

	public void setStatavX(String statavX) {
		this.statavX = statavX;
	}

	public String getTpX() {
		return tpX;
	}

	public void setTpX(String tpX) {
		this.tpX = tpX;
	}

	public String getStatX() {
		return statX;
	}

	public void setStatX(String statX) {
		this.statX = statX;
	}

	public String getNbsalX() {
		return nbsalX;
	}

	public void setNbsalX(String nbsalX) {
		this.nbsalX = nbsalX;
	}

	public String getEmplX() {
		return emplX;
	}

	public void setEmplX(String emplX) {
		this.emplX = emplX;
	}

	public String getPospX() {
		return pospX;
	}

	public void setPospX(String pospX) {
		this.pospX = pospX;
	}

	public String getFoncX() {
		return foncX;
	}

	public void setFoncX(String foncX) {
		this.foncX = foncX;
	}

	public String getiRepriseProfQ() {
		return iRepriseProfQ;
	}

	public void setiRepriseProfQ(String iRepriseProfQ) {
		this.iRepriseProfQ = iRepriseProfQ;
	}

	public String getProfaX() {
		return profaX;
	}

	public void setProfaX(String profaX) {
		this.profaX = profaX;
	}

	public String getProfaLM() {
		return profaLM;
	}

	public void setProfaLM(String profaLM) {
		this.profaLM = profaLM;
	}

	public String getProfaC() {
		return profaC;
	}

	public void setProfaC(String profaC) {
		this.profaC = profaC;
	}

	public String getProfaLMQ() {
		return profaLMQ;
	}

	public void setProfaLMQ(String profaLMQ) {
		this.profaLMQ = profaLMQ;
	}

	public String getProfaCMQ() {
		return profaCMQ;
	}

	public void setProfaCMQ(String profaCMQ) {
		this.profaCMQ = profaCMQ;
	}

	public String getProfaLMArb() {
		return profaLMArb;
	}

	public void setProfaLMArb(String profaLMArb) {
		this.profaLMArb = profaLMArb;
	}

	public String getProfaCMArb() {
		return profaCMArb;
	}

	public void setProfaCMArb(String profaCMArb) {
		this.profaCMArb = profaCMArb;
	}

	public String getProfiX() {
		return profiX;
	}

	public void setProfiX(String profiX) {
		this.profiX = profiX;
	}

	public String getProfiLM() {
		return profiLM;
	}

	public void setProfiLM(String profiLM) {
		this.profiLM = profiLM;
	}

	public String getProfiC() {
		return profiC;
	}

	public void setProfiC(String profiC) {
		this.profiC = profiC;
	}

	public String getProfiLMQ() {
		return profiLMQ;
	}

	public void setProfiLMQ(String profiLMQ) {
		this.profiLMQ = profiLMQ;
	}

	public String getProfiCMQ() {
		return profiCMQ;
	}

	public void setProfiCMQ(String profiCMQ) {
		this.profiCMQ = profiCMQ;
	}

	public String getProfiLMArb() {
		return profiLMArb;
	}

	public void setProfiLMArb(String profiLMArb) {
		this.profiLMArb = profiLMArb;
	}

	public String getProfiCMArb() {
		return profiCMArb;
	}

	public void setProfiCMArb(String profiCMArb) {
		this.profiCMArb = profiCMArb;
	}

	public String getProfsX() {
		return profsX;
	}

	public void setProfsX(String profsX) {
		this.profsX = profsX;
	}

	public String getProfsLM() {
		return profsLM;
	}

	public void setProfsLM(String profsLM) {
		this.profsLM = profsLM;
	}

	public String getProfsC() {
		return profsC;
	}

	public void setProfsC(String profsC) {
		this.profsC = profsC;
	}

	public String getProfsLMQ() {
		return profsLMQ;
	}

	public void setProfsLMQ(String profsLMQ) {
		this.profsLMQ = profsLMQ;
	}

	public String getProfsCMQ() {
		return profsCMQ;
	}

	public void setProfsCMQ(String profsCMQ) {
		this.profsCMQ = profsCMQ;
	}

	public String getProfsLMArb() {
		return profsLMArb;
	}

	public void setProfsLMArb(String profsLMArb) {
		this.profsLMArb = profsLMArb;
	}

	public String getProfsCMArb() {
		return profsCMArb;
	}

	public void setProfsCMArb(String profsCMArb) {
		this.profsCMArb = profsCMArb;
	}

	public String getiCltMQ() {
		return iCltMQ;
	}

	public void setiCltMQ(String iCltMQ) {
		this.iCltMQ = iCltMQ;
	}

	public String getDltCMQ() {
		return dltCMQ;
	}

	public void setDltCMQ(String dltCMQ) {
		this.dltCMQ = dltCMQ;
	}

	public String getiActetQ() {
		return iActetQ;
	}

	public void setiActetQ(String iActetQ) {
		this.iActetQ = iActetQ;
	}

	public String getiSiretQ() {
		return iSiretQ;
	}

	public void setiSiretQ(String iSiretQ) {
		this.iSiretQ = iSiretQ;
	}

	public String getiProfsQ() {
		return iProfsQ;
	}

	public void setiProfsQ(String iProfsQ) {
		this.iProfsQ = iProfsQ;
	}

	public String getiProfiQ() {
		return iProfiQ;
	}

	public void setiProfiQ(String iProfiQ) {
		this.iProfiQ = iProfiQ;
	}

	public String getiProfaQ() {
		return iProfaQ;
	}

	public void setiProfaQ(String iProfaQ) {
		this.iProfaQ = iProfaQ;
	}

	public String getDrCodeRecap() {
		return drCodeRecap;
	}

	public void setDrCodeRecap(String drCodeRecap) {
		this.drCodeRecap = drCodeRecap;
	}

	public String getIdepRecap() {
		return idepRecap;
	}

	public void setIdepRecap(String idepRecap) {
		this.idepRecap = idepRecap;
	}

	public Integer getPoidsBi() {
		return poidsBi;
	}

	public void setPoidsBi(Integer poidsBi) {
		this.poidsBi = poidsBi;
	}

	public String getCltCC() {
		return cltCC;
	}

	public void setCltCC(String cltCC) {
		this.cltCC = cltCC;
	}

	public String getiCltC() {
		return iCltC;
	}

	public void setiCltC(String iCltC) {
		this.iCltC = iCltC;
	}

	public String getDltCC() {
		return dltCC;
	}

	public void setDltCC(String dltCC) {
		this.dltCC = dltCC;
	}

	public String getCommentaireActivite() {
		return commentaireActivite;
	}

	public void setCommentaireActivite(String commentaireActivite) {
		this.commentaireActivite = commentaireActivite;
	}

	public Integer getLotIdQSauv() {
		return lotIdQSauv;
	}

	public void setLotIdQSauv(Integer lotIdQSauv) {
		this.lotIdQSauv = lotIdQSauv;
	}

	public LotsRepriseQ getLotsRepriseQ() {
		return lotsRepriseQ;
	}

	public void setLotsRepriseQ(LotsRepriseQ lotsRepriseQ) {
		this.lotsRepriseQ = lotsRepriseQ;
	}

}
