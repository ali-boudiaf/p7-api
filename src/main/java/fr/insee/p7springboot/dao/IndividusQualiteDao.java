package fr.insee.p7springboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.insee.p7springboot.model.IndividusQualite;
import fr.insee.p7springboot.model.pk.IndividusQualitePK;

@Repository
public interface IndividusQualiteDao extends JpaRepository<IndividusQualite, IndividusQualitePK> {
//	public List<IndividusQualite> findAll();
//	public IndividusQualite findById(int id);
//    public IndividusQualite save(IndividusQualite individusQualite);
//    public List<IndividusQualite> findByNomLikeIgnoreCase(String recherche);
}
