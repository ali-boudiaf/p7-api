package fr.insee.p7springboot.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.insee.p7springboot.model.LotsRepriseQ;

@Repository
public interface LotsRepriseQDao extends JpaRepository<LotsRepriseQ, Integer> {

	@Query("FROM LotsRepriseQ WHERE idep=:idep")
	Set<LotsRepriseQ> findByIdep(@Param(value = "idep") String idep);

	@Query("FROM LotsRepriseQ WHERE idep is null")
	Set<LotsRepriseQ> recupererLesLotsNonAffectes();

	@Query(value = "select * FROM rp.lots_reprise_q WHERE idep is null LIMIT 1", nativeQuery = true)
	LotsRepriseQ recupererUnLotNonAffecte();
}
