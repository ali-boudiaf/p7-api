package fr.insee.p7springboot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.insee.p7springboot.model.DrInsee;

@Repository
public interface DrInseeDao extends JpaRepository<DrInsee, String> {

}
