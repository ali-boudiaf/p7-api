package fr.insee.p7springboot.web.controller;

import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.insee.p7springboot.model.IndividusQualite;
import fr.insee.p7springboot.model.pk.IndividusQualitePK;
import fr.insee.p7springboot.service.IndividusQualiteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("API pour les opérations CRUD sur les individus_qualite")
@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class IndividusQualiteController {

	@Autowired
	IndividusQualiteService individusQualiteService;

	/*
	 * Récupérer la liste des individus_qualité
	 */
	@ApiOperation(value = "Récupère l'ensemble des individus_qualite")
	@RequestMapping(value = "/individus-qualite", method = RequestMethod.GET)
	public MappingJacksonValue listeIndividusQualite() {

		Iterable<IndividusQualite> individusQualiteAll = individusQualiteService.getDao().findAll();

		MappingJacksonValue individusQualiteAllFiltres = new MappingJacksonValue(individusQualiteAll);

		return individusQualiteAllFiltres;

	}

//	@GetMapping(value = "/individus-qualite/nom/{recherche}")
//	public List<IndividusQualite> testDeRequete(@PathVariable String recherche) {
//		return individusQualiteDao.findByNomLikeIgnoreCase("%" + recherche + "%");
//	}

	/*
	 * Récupérer un individus par son Id
	 */
	@ApiOperation(value = "Récupère un individus_qualite en utilisant l'ID comme critère de recherche")
	@GetMapping(value = "/individus-qualite/id/{id}")
	public IndividusQualite afficherUnIndividusQualite(@PathVariable int id) {

		IndividusQualite monIndivQualite = individusQualiteService.getDao()
				.findById(new IndividusQualitePK("10", "121", "0001", 4, 1, 2, "AAM")).orElse(null);

		return monIndivQualite;
	}

	@ApiOperation(value = "Ajoute un individus_qualite en base de donnees")
	@PostMapping(value = "/individus-qualite")
	public ResponseEntity<Void> ajouterIndividus(@RequestBody IndividusQualite individusQualite) {

		IndividusQualite individusAdded = individusQualiteService.getDao().save(individusQualite);

		if (individusAdded == null) {
			return ResponseEntity.noContent().build();
		}

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(individusAdded)
				.toUri();

		return ResponseEntity.created(location).build();
	}

}
