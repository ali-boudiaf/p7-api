package fr.insee.p7springboot.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import fr.insee.p7springboot.model.DrInsee;
import fr.insee.p7springboot.service.DrInseeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("API pour les opérations CRUD sur les dr_insee")
@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class DrInseeController {

	@Autowired
	DrInseeService drInseeService;

	/*
	 * Récupérer la liste des dr_insee
	 */

	/*
	 * Récupérer une dr_insee par son Id
	 */
	@ApiOperation(value = "Récupère une dr-insee en utilisant l'Id comme critère de recherche")
	@GetMapping(value = "/dr-insee/id/{id}")
	public DrInsee afficherUneDrInsee(@PathVariable String id) {
		return drInseeService.getDao().findById("DR13").orElse(null);
	}

}
