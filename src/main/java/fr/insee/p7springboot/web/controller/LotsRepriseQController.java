package fr.insee.p7springboot.web.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import fr.insee.p7springboot.model.LotsRepriseQ;
import fr.insee.p7springboot.service.LotsRepriseQService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("API pour les opérations CRUD sur les lots_reprise_q")
@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class LotsRepriseQController {

	@Autowired
	LotsRepriseQService lotsRepriseQService;

	/*
	 * Récupérer un lot-reprise_q par son Id
	 */
	@ApiOperation(value = "Récupère un lots_reprise_q en utilisant l'ID comme critère de recherche")
	@GetMapping(value = "/lots-reprise-q/id/{id}")
	public LotsRepriseQ afficherUnLotRepriseQ(@PathVariable int id) {
		return lotsRepriseQService.getDao().findById(id).orElse(null);
	}

	/*
	 * Récupération d'un lot en utilisant l'idep de l'agent
	 */
	@ApiOperation(value = "Récupère les lots_reprise_q en utilisant l'Idep comme critère de recherche")
	@GetMapping(value = "/lots-reprise-q/idep/{idep}")
	public Set<LotsRepriseQ> afficherLesLotsRepriseQByIdep(@PathVariable String idep) {
		return lotsRepriseQService.getDao().findByIdep(idep.toUpperCase());
	}

	/*
	 * Récupération des lots non affectees
	 */
	@ApiOperation(value = "Récupère les lots_reprise_q non affecte")
	@GetMapping(value = "/lots-reprise-q/nonaffectes/")
	public Set<LotsRepriseQ> recupererLesLotsNonAffectes() {
		return lotsRepriseQService.getDao().recupererLesLotsNonAffectes();
	}

	/*
	 * Récupération des lots non affectees
	 */
	@ApiOperation(value = "Récupère un lot_reprise_q non affecte")
	@GetMapping(value = "/lots-reprise-q/nonaffecte/{idep}")
	public LotsRepriseQ recupererUnLotRepriseQ(@PathVariable String idep) {
		// methode 1 : récuperer les lots de l'idep et vérifier <5, si oui alors donner
		// un lot // DAO @idep -> list
		// methode 2 : à partir de l'idep, créer un objet utilisateur qui a une liste de
		// lot // JAVA-metier/domaine/dto
		LotsRepriseQ monLotsRepriseQ = lotsRepriseQService.getDao().recupererUnLotNonAffecte();
		monLotsRepriseQ.setIdep(idep.toUpperCase());
		lotsRepriseQService.getDao().save(monLotsRepriseQ);
		return monLotsRepriseQ;
	}
}
