package fr.insee.p7springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class P7SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(P7SpringBootApplication.class, args);
	}

}
