package fr.insee.p7springboot.integrationtest;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class RecupererIndividusTest {
	
	static final String BASEURL = "http://localhost:9090";
	private String endpoint = "/individus-qualite/";

	@Test
	public void testRecupIndividus() throws IOException {
		//GIVEN
		OkHttpClient client = new OkHttpClient();
		
		//WHEN
		Request request = new Request.Builder()
	            .url(BASEURL+endpoint)
	            .build();
		Response response = client.newCall(request).execute();
		
		//THEN
		Assert.assertEquals("Toto", response.body().string());
	}

}
