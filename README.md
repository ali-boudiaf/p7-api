Pour tester les microservices se rendre sur:

http://localhost:9090/swagger-ui.html#/

Pour tester la base de données se rendre sur:

http://localhost:9090/h2-console/

Dans le champ JDBC URL, saisir:

jdbc:h2:mem:testdb
